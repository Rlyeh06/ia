#include "Ia.h"
#include <time.h>
#include <stdio.h>
#include <iostream>

void Ia::humain(Jeu *game, int i) {
    game->jouerCase(i);
}

void Ia::random(Jeu *game) {
    //std::cout << "seed" << std::endl;
    game->getCaseLibre();
    int max = game->getNbCaseLibre();
    int rand = (std::rand() % (int) max);
    //std::cout << "rand" << std::endl;
    int output = game->numCaseLibre(rand);
    //std::cout << "ouput" << std::endl;
    game->jouerCase(output);
    //std::cout << "case jouee" << std::endl;
}

void Ia::minMax(Jeu *game) {
    int restant = game->restant;
    int dimention = game->dimention;
    int profondeur;
    if (restant == 25) {
        game->jouerCase(12);
    } else if ((dimention == 3)&&(restant == 9)) {
        game->jouerCase(dimention - 1);
    } else if ((dimention == 3)&&(restant == 8)&&(game->caseVide(4))) {
        game->jouerCase(4);
    } else {
        if (restant >= 22) {
            profondeur = 4;
        } else if (restant >= 17) {
            profondeur = 5;
        } else if (restant >= 10) {
            profondeur = 6;
        } else if (restant >= 7) {
            profondeur = restant;
        }
        int i;
        int score;
        int max = -999999;
        int maxi = -1;
        calcIa->ia = game->joueur;
        if ((profondeur >= 0)&&(game->modejeu == 1)) {
            for (i = 0; i < (dimention * dimention); i++) {
                if (game->caseVide(i)) {
                    game->jouerCase(i);
                    score = calcIa->minMM(game, profondeur - 1);
                    if (score >= max) {
                        max = score;
                        maxi = i;
                    }
                    game->annulerCase(i);
                }
            }
        }
        game->jouerCase(maxi);
    }
}

void Ia::alphaBeta(Jeu *game) {
    int restant = game->restant;
    int dimention = game->dimention;
    int profondeur;
    if (restant >= 25) {
        profondeur = 3;
    } else if (restant >= 10) {
        profondeur = 4;
    } else {
        profondeur = 5;
    }
    int i;
    int score;
    int maxi = -1;
    int alpha = -999999;
    int beta = 999999;
    calcIa->ia = game->joueur;
    if ((profondeur >= 0)&&(game->modejeu == 1)) {
        for (i = 0; i < (dimention * dimention); i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                score = calcIa->minAB(game, profondeur - 1, alpha, beta);
                if (alpha < score) {
                    alpha = score;
                    maxi = i;
                }
                game->annulerCase(i);
            }
        }
    }
    game->jouerCase(maxi);
}

void Ia::competition(Jeu *game) {
    int restant = game->restant;
    int profondeur;
    if (restant == 100) {
        game->jouerCase(44);
    }else if(restant == 99){
        if(game->caseVide(44)){
            game->jouerCase(44);
        }else if(game->caseVide(45)){
            game->jouerCase(45);
        }
    } else {
        if (restant >= 25) {
            profondeur = 3;
        } else if (restant >= 10) {
            profondeur = 4;
        } else {
            profondeur = 5;
        }
        //std::cout << profondeur << std::endl;
        int i;
        int score;
        int maxi = -1;
        int alpha = -999999;
        int beta = 999999;
        calcIa->ia = game->joueur;
        if ((profondeur >= 0)&&(game->modejeu == 1)) {
            for (i = 0; i < 100; i++) {
                if (game->caseVide(i)) {
                    game->jouerCase(i);
                    score = calcIa->minABcompet(game, profondeur - 1, alpha, beta);
                    if (alpha < score) {
                        alpha = score;
                        maxi = i;
                    }
                    game->annulerCase(i);
                }
            }
        }
        game->jouerCase(maxi);
    }
}