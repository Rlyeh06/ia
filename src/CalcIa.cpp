#include "CalcIa.h"

void CalcIa::setIa(Jeu *game) {
    ia = game->joueur;
}

int CalcIa::score(nombrePion tmp) {
    return tmp.nbPion * tmp.nbPionJoueur;
}

nombrePion CalcIa::evaluCase(int typeCase, int serie) {
    nombrePion tmp;
    tmp.nbPion = 1;
    if (serie >= 5) {
        serie = 10;
    }
    if (typeCase == ia)
        tmp.nbPionJoueur = serie;
    else
        tmp.nbPionJoueur = -serie;
    return tmp;
}
nombrePion CalcIa::evaluCaseCompet(int typeCase, int serie) {
    nombrePion tmp;
    tmp.nbPion = 1;
    if (serie >= 5) {
        serie = 10;
    }
    if (typeCase == ia)
        tmp.nbPionJoueur = serie;
    else
        tmp.nbPionJoueur = -serie;
    return tmp;
}

int CalcIa::evaluJeu(Jeu *game) {
    nombrePion pion;
    nombrePion ligne;
    int jouer = ia;
    int nbgagne = game->nbgagne;
    int dimention = game->dimention;
    int scorePlateau = 0;
    if (game->modejeu != 1) {
        if (game->gagnant == ia) {
            return 999999 - (game->numPions()*100);
        } else if (game->gagnant == 0) {
            return 0;
        } else {
            return -999999 + (game->numPions()*100);
        }
    }
    jouer=game->joueur;
    int serie = 0;
    for (int numcase = 0; numcase < dimention; numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, 0, -1, serie); //toutes colonnes
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = 0; numcase < dimention; numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, 1, -1, serie); //toutes diag sup (gauche a droite)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = 0; numcase < dimention; numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, -1, -1, serie); //toutes diag sup (droite a gauche)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = 0; numcase <= (dimention * dimention - dimention); numcase += dimention) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, 1, 0, serie); //toutes lignes
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = (dimention * dimention - dimention + 1); numcase < (dimention * dimention - 1); numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, 1, 1, serie); //toutes diag inf (gauche a droite)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = (dimention * dimention - dimention + 1); numcase < (dimention * dimention - 1); numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCell(game, numcase, nbgagne, jouer, -1, 1, serie); //toutes diag inf (droite a gauche)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    return scorePlateau;
}

int CalcIa::evaluJeuCompet(Jeu *game) {
    nombrePion pion;
    nombrePion ligne;
    int jouer = ia;
    int nbgagne = 5;
    int scorePlateau = 0;
    if (game->modejeu != 1) {
        if (game->gagnant == ia) {
            return 999999 - (game->numPions()*100);
        } else if (game->gagnant == 0) {
            return 0;
        } else {
            return -999999 + (game->numPions()*100);
        }
    }
    int serie = 0;
    for (int numcase = 0; numcase < 10; numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, 0, -1, serie); //toutes colonnes
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, 1, -1, serie); //toutes diag sup (gauche a droite)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, -1, -1, serie); //toutes diag sup (droite a gauche)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = 0; numcase <= 99; numcase += 10) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, 1, 0, serie); //toutes lignes
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    for (int numcase = 91; numcase < 99; numcase++) {
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, 1, 1, serie); //toutes diag inf (gauche a droite)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
        pion.nbPion = 0;
        pion.nbPionJoueur = 0;
        ligne = evaluationCellCompet(game, numcase, nbgagne, jouer, -1, 1, serie); //toutes diag inf (droite a gauche)
        pion.nbPion += ligne.nbPion;
        pion.nbPionJoueur += ligne.nbPionJoueur;
        scorePlateau += score(pion);
    }
    return scorePlateau;
}
nombrePion CalcIa::evaluationCellCompet(Jeu *game, int numcase, int profondeur, int jouer, int dx, int dy, int serie) {
    nombrePion tmp;
    nombrePion tmp1;
    tmp.nbPion = 0;
    tmp.nbPionJoueur = 0;
    int testCase = game->getTypeCase(numcase);
    int stopRecursion = (((dx == 1)&&(((numcase + 1) %10) == 0)) ||
            ((dx == -1)&&((numcase %10) == 0)) ||
            ((dy == 1)&&((numcase <= 9))) ||
            ((dy == -1)&&(numcase >= 90)) ||
            (profondeur == 1)); //stop recursion if =1;
    if ((testCase == 0)&&(stopRecursion == 1)) {
        return tmp;
    } else if (testCase == 0) {
        int numcasefils = (numcase + dx)-(10 * dy);
        tmp1 = evaluationCellCompet(game, numcasefils, profondeur, jouer, dx, dy, 1);
        tmp.nbPion += tmp1.nbPion;
        tmp.nbPionJoueur += tmp1.nbPionJoueur;
        return tmp;
    } else if (stopRecursion == 1) {
        return evaluCaseCompet(testCase, serie);
    } else {
        int numcasefils = (numcase + dx)-(10 * dy);
        int caseFils = game->getTypeCase(numcasefils);
        if ((caseFils == 0)&&(serie!=0)){
            serie++;
        }
        else if(caseFils == testCase){
            serie++;
        }else {
            serie = 0;
        }
        tmp = evaluCaseCompet(testCase, serie);
        tmp1 = evaluationCellCompet(game, numcasefils, profondeur, jouer, dx, dy, serie);
        tmp.nbPion += tmp1.nbPion;
        tmp.nbPionJoueur += tmp1.nbPionJoueur;
        return tmp;
    }
}
nombrePion CalcIa::evaluationCell(Jeu *game, int numcase, int profondeur, int jouer, int dx, int dy, int serie) {
    int dimention = game->dimention;
    nombrePion tmp;
    nombrePion tmp1;
    tmp.nbPion = 0;
    tmp.nbPionJoueur = 0;
    int testCase = game->getTypeCase(numcase);
    int stopRecursion = (((dx == 1)&&(((numcase + 1) % dimention) == 0)) ||
            ((dx == -1)&&((numcase % dimention) == 0)) ||
            ((dy == 1)&&((numcase <= (dimention - 1)))) ||
            ((dy == -1)&&(numcase >= (dimention * dimention) - dimention)) ||
            (profondeur == 1)); //stop recursion if =1;
    if ((testCase == 0)&&(stopRecursion == 1)) {
        return tmp;
    } else if (testCase == 0) {
        int numcasefils = (numcase + dx)-(dimention * dy);
        tmp1 = evaluationCell(game, numcasefils, profondeur, jouer, dx, dy, 1);
        tmp.nbPion += tmp1.nbPion;
        tmp.nbPionJoueur += tmp1.nbPionJoueur;
        return tmp;
    } else if (stopRecursion == 1) {
        return evaluCase(testCase, serie);
    } else {
        int numcasefils = (numcase + dx)-(dimention * dy);
        int caseFils = game->getTypeCase(numcasefils);
        if ((caseFils == testCase) || (caseFils == 0)) {
            serie++;
        } else {
            serie = 1;
        }
        tmp = evaluCase(testCase, serie);
        tmp1 = evaluationCell(game, numcasefils, profondeur, jouer, dx, dy, serie);
        tmp.nbPion += tmp1.nbPion;
        tmp.nbPionJoueur += tmp1.nbPionJoueur;
        return tmp;
    }
}

int CalcIa::minMM(Jeu *game, int profondeur) {
    int i;
    int tmp;
    int min = 999999;
    int dimention = game->dimention;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeu(game);
    } else {
        for (i = 0; i < (dimention * dimention); i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = maxMM(game, profondeur - 1);
                if (tmp < min) {
                    min = tmp;
                }
                game->annulerCase(i);
            }
        }
        return min;
    }
}

int CalcIa::maxAB(Jeu *game, int profondeur, int alpha, int beta) {
    int i;
    int tmp;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeu(game);
    } else {
        for (i = 0; i < (game->dimention * game->dimention); i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = minAB(game, profondeur - 1, alpha, beta);
                game->annulerCase(i);
                if (alpha < tmp) {
                    alpha = tmp;
                }
                if (beta <= alpha) {
                    return alpha;
                }
            }
        }
        return alpha;
    }
}

int CalcIa::minAB(Jeu *game, int profondeur, int alpha, int beta) {
    int i;
    int tmp;
    int dimention = game->dimention;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeu(game);
    } else {
        for (i = 0; i < (dimention * dimention); i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = maxAB(game, profondeur - 1, alpha, beta);
                game->annulerCase(i);
                if (beta > tmp) {
                    beta = tmp;
                }
                if (beta <= alpha) {
                    return beta;
                }
            }
        }
        return beta;
    }
}

int CalcIa::maxABcompet(Jeu *game, int profondeur, int alpha, int beta) {
    int i;
    int tmp;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeuCompet(game);
    } else {
        for (i = 0; i < 100; i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = maxABcompet(game, profondeur - 1, alpha, beta);
                game->annulerCase(i);
                if (beta > tmp) {
                    beta = tmp;
                }
                if (beta <= alpha) {
                    return beta;
                }
            }
        }
        return beta;
    }
}

int CalcIa::minABcompet(Jeu *game, int profondeur, int alpha, int beta) {
    int i;
    int tmp;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeuCompet(game);
    } else {
        for (i = 0; i < 100; i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = maxABcompet(game, profondeur - 1, alpha, beta);
                game->annulerCase(i);
                if (beta > tmp) {
                    beta = tmp;
                }
                if (beta <= alpha) {
                    return beta;
                }
            }
        }
        return beta;
    }
}

int CalcIa::maxMM(Jeu *game, int profondeur) {
    int i;
    int tmp;
    int max = -999999;
    if ((profondeur == 0) || (game->modejeu != 1)) {
        return evaluJeu(game);
    } else {
        for (i = 0; i < (game->dimention * game->dimention); i++) {
            if (game->caseVide(i)) {
                game->jouerCase(i);
                tmp = minMM(game, profondeur - 1);
                if (tmp > max) {
                    max = tmp;
                }
                game->annulerCase(i);
            }
        }
        return max;
    }
}