#include <iostream>
#include "Jeu.h"
#include "Engine.h"
#include "MyGraphicEngine.h"
#include "MyGameEngine.h"
#include "MyControlEngine.h"

int main(int argc, char * argv[]) {
    srand(time(NULL));
    Engine e(argc, argv);
    std::vector<Case *> cases;
    std::vector<int> casesLibres;
    int dimention = 0;
    int nbgagne = 3;
    int modejeu = -11;
    int joueur = 1;
    CalcIa calcia = CalcIa(0);
    Ia ia = Ia(&calcia);
    Jeu game = Jeu(&cases,&casesLibres,800, 600, dimention, nbgagne, modejeu, joueur);
    GameEngine * gme = new MyGameEngine(&game,&ia);
    ControlEngine * ce = new MyControlEngine(&game,&ia);
    GraphicEngine * ge = new MyGraphicEngine(&game,&ia);
    e.setGraphicEngine(ge);
    e.setGameEngine(gme);
    e.setControlEngine(ce);
    e.start();
    return 0;
}
