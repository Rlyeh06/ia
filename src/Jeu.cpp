#include "Jeu.h"
#include <stdio.h>
#include <iostream>

void Jeu::initCase() {
    float y = 1.0f;
    float x = -1.0f;
    float taille = 2.0f / (float) dimention;
    for (int i = 0; i < dimention; i++) {
        x = -1.0f;
        y -= taille;
        for (int j = 0; j < dimention; j++) {
            cases->push_back(new Case(x, y, taille));
            x += taille;
        }

    }
}

void Jeu::reset() {
    for (int i = 0; i < cases->size(); i++) {
        setTypeCase(i, 0);
    }
    while (cases->size() >= 1) {
        cases->erase(cases->begin());
    }
}

void Jeu::getCaseLibre() {
    casesLibres->clear();
    for (int i = 0; i < (dimention * dimention); i++) {
        if (getTypeCase(i) == 0) {
            casesLibres->push_back(i);
        }
    }
}

int Jeu::getNbCaseLibre() {
    return casesLibres->size();
}

int Jeu::numCaseLibre(int i) {
    return casesLibres->at(i);
}

void Jeu::draw() {
    for (int i = 0; i < cases->size(); i++) {
        (*cases)[i]->draw();
    }
    float taille = (*cases)[1]->taille;
    int dim = dimention - 1;
    for (int i = 1; i <= dim; i++) {
        GraphicPrimitives::drawLine2D(-1.0f + (i * taille), -1.0f, -1.0f + (i * taille), 1.0f, 1.0f, 1.0f, 1.0f, 0.0f);
        GraphicPrimitives::drawLine2D(-1.0f, -1.0f + (i * taille), 1.0f, -1.0f + (i * taille), 1.0f, 1.0f, 1.0f, 0.0f);
    }
}

void Jeu::setMode(int i) {
    modejeu = i;
}
int Jeu::caseVide(int i) {
    int j = getTypeCase(i);
    if (j == 0) {
        return 1;
    } else {
        return 0;
    }
}

int Jeu::getTypeCase(int i) {
    return (*cases)[i]->type;
}

void Jeu::setTypeCase(int i, int j) {
    (*cases)[i]->setType(j);
}

void Jeu::annulerCase(int i) {
    setTypeCase(i, 0);
    if (modejeu != 1) {
        setMode(1);
        setGagnant(0);
    }
    restant++;
    changeJoueur();
}

void Jeu::jouerCase(int i) {
    //std::cout << "joueur case" << std::endl;
    if ((getTypeCase(i)) == 0) {
        //std::cout << "case libre" << std::endl;
        setTypeCase(i, joueur);
        int j = gagne(i, joueur);
        if (j == 1) {
            setMode(0 - joueur);
            setGagnant(joueur);
            changeJoueur();
            caseRestant();
        } else {
            caseRestant();
            //std::cout << "maj case" << std::endl;
            changeJoueur();
        }
    }
}

int Jeu::numPions() {
    return (dimention * dimention)-restant;
}

void Jeu::caseRestant() {
    restant--;
    if (restant == 0) {
        modejeu = -3;
        gagnant = 0;
    }
}

void Jeu::setGagnant(int i) {
    gagnant = i;
}

void Jeu::changeJoueur() {
    //std::cout << "joueur changer" << std::endl;
    if (joueur == 1) {
        joueur = 2;
        //std::cout << "j2" << std::endl;
    } else if (joueur == 2) {
        joueur = 1;
        //std::cout << "j1" << std::endl;
    }
}

void Jeu::setJoueurCommence(int i) {
    joueur = i;
}

void Jeu::setDimention(int i) {
    dimention = i;
}

void Jeu::setNbgagne(int i) {
    nbgagne = i;
}

void Jeu::initRestant() {
    restant = dimention*dimention;
}

void Jeu::setJoueur1(int i) {//1=main,2=iarand,3=min-max,4=alpha-beta
    j1 = i;
}

void Jeu::setJoueur2(int i) {
    j2 = i;
}

int Jeu::gagne(int numcase, int jouer) {
    //std::cout << "origine" << std::endl;
    //std::cout << numcase << std::endl;
    int res1 = 1; //verticale
    int res2 = 1; //horizontale
    int res3 = 1; //slash
    int res4 = 1; //backslash
    if (numcase >= dimention) {//pas 1er ligne
        //std::cout << "haut" << std::endl;
        res1 += checkCell(numcase, nbgagne, jouer, 0, 1);
    }
    if (numcase < ((dimention * dimention) - dimention)) {//pas derniere ligne
        //std::cout << "bas" << std::endl;
        res1 += checkCell(numcase, nbgagne, jouer, 0, -1);
    }
    if (res1 >= nbgagne){
        return 1;
    }
    if ((numcase % dimention) != 0) {//pas 1ere colonne
        //std::cout << "gauche" << std::endl;
        res2 += checkCell(numcase, nbgagne, jouer, -1, 0);
    }
    if (((numcase + 1) % dimention) != 0) {//pas derniere colonne
        //std::cout << "droite" << std::endl;
        res2 += checkCell(numcase, nbgagne, jouer, 1, 0);
    }
    if (res2 >= nbgagne){
        return 1;
    }
    if ((numcase >= dimention)&&(((numcase + 1) % dimention) != 0)) {//pas coin haut droite
        //std::cout << "haut droite" << std::endl;
        res3 += checkCell(numcase, nbgagne, jouer, 1, 1);
    }
    if (((numcase % dimention) != 0)&&(numcase < ((dimention * dimention) - dimention))) {//pas coin bas gauche
        //std::cout << "bas gauche" << std::endl;
        res3 += checkCell(numcase, nbgagne, jouer, -1, -1);
    }
    if (res3 >= nbgagne){
        return 1;
    }
    if ((numcase >= dimention)&&((numcase % dimention) != 0)) {//pas coin haut gauche
        //std::cout << "haut gauche" << std::endl;
        res4 += checkCell(numcase, nbgagne, jouer, -1, 1);
    }
    if ((((numcase + 1) % dimention) != 0)&&(numcase < ((dimention * dimention) - dimention))) {//pas coin bas droite
        //std::cout << "bas droite" << std::endl;
        res4 += checkCell(numcase, nbgagne, jouer, 1, -1);
    }
    if (res4 >= nbgagne){
        return 1;
    }
    return 0;
}

int Jeu::checkCell(int numcase, int profondeur, int jouer, int dx, int dy) {
    numcase = (numcase + dx)-(dimention * dy);
    //std::cout << numcase << std::endl;
    if (((dx == 1)&&(((numcase + 1) % dimention) == 0)) ||
            ((dx == -1)&&((numcase % dimention) == 0)) ||
            ((dy == 1)&&((numcase <= (dimention - 1)))) ||
            ((dy == -1)&&(numcase >= (dimention * dimention) - dimention)) ||
            (profondeur == 1)) {//stop recursion
        if (getTypeCase(numcase) == jouer) {
            return 1;
        } else {
            return 0;
        }
    }
    if (getTypeCase(numcase) == jouer) {
        return 1 + checkCell(numcase, profondeur, jouer, dx, dy);
    } else {
        return 0;
    }
}