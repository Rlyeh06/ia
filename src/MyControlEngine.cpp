#include "MyControlEngine.h"
#include <stdio.h>

void MyControlEngine::MouseCallback(int button, int state, int x, int y) {
    int width = game->wWIDTH;
    int height = game->wHEIGHT;
    int modejeu = game->modejeu;
    int joueur = game->joueur;

    if (modejeu == 1) {
        int dimention = (game->dimention);
        int x1 = (width / dimention);
        int x2 = x / x1;
        int y1 = (height / dimention) - 1;
        int y2 = (y / y1);
        int y3 = (dimention * y2);
        int i = x2 + y3;
        if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
            ia->humain(game, i);
        }
    }
}

void MyControlEngine::KeyboardCallback(unsigned char key, int x, int y) {
    int modejeu = game->modejeu;
    if (modejeu == -11) {
        if (key == '1') {
            game->setDimention(0);
        } else if (key == '2') {
            game->setDimention(10);
            game->setNbgagne(5);
            game->setJoueur1(5);
            game->setJoueur2(1);
        } else if (key == 13) {
            if (game->dimention == 0) {
                game->setDimention(3);
                game->setMode(-10);
            } else {
                game->initRestant();
                game->setMode(-7);
            }
        }
    } else if (modejeu == -10) {
        if (key == '1') {
            game->setDimention(3);
            game->setNbgagne(3);
        } else if (key == '2') {
            game->setDimention(5);
            game->setNbgagne(4);
        } else if (key == '3') {
            game->setDimention(10);
            game->setNbgagne(5);
        } else if (key == 13) {
            game->initRestant();
            game->setMode(-9);
        }
    } else if (modejeu == -9) {
        if (key == '1') {
            game->setJoueur1(1);
        } else if (key == '2') {
            game->setJoueur1(2);
        } else if (key == '3') {
            game->setJoueur1(3);
        } else if (key == '4') {
            game->setJoueur1(4);
        } else if (key == 13) {
            game->setMode(-8);
        }
    } else if (modejeu == -8) {
        if (key == '1') {
            game->setJoueur2(1);
        } else if (key == '2') {
            game->setJoueur2(2);
        } else if (key == '3') {
            game->setJoueur2(3);
        } else if (key == '4') {
            game->setJoueur2(4);
        } else if (key == 13) {
            game->setMode(-7);
        }
    } else if (modejeu == -7) {
        if (key == '1') {
            game->setJoueurCommence(1);
        } else if (key == '2') {
            game->setJoueurCommence(2);
        } else if (key == 13) {
            game->setMode(0);
        }
    } else if ((modejeu == -1) || (modejeu == -2) || (modejeu == -3)) {
        game->reset();
        game->setMode(-11);
    }
}
