
#include "MyGraphicEngine.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <cstring>
using namespace std;

void MyGraphicEngine::reshape(int winWidth_, int winHeight_) {
    game->wWIDTH = winWidth_;
    game->wHEIGHT = winHeight_;
}

void MyGraphicEngine::Draw() {
    int modejeu = game->modejeu;
    if (modejeu == -11) {
        string strm1 = "Competition:";
        string strm2;
        string strm3;
        string strm4;
        if (game->dimention == 0) {
            strm2 = "*[1]Non";
            strm3 = " [2]Oui";
        } else if (game->dimention == 10) {
            strm2 = " [1]Non";
            strm3 = "*[2]Oui";
        }
        GraphicPrimitives::drawText2D(&strm1[0], 0.0f - 0.1f, 0.6f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm2[0], 0.0f - 0.1f, 0.4f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm3[0], 0.0f - 0.1f, 0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (modejeu == -10) {
        string strm1 = "Dimension de la grille:";
        string strm2;
        string strm3;
        string strm4;
        if (game->dimention == 3) {
            strm2 = "*[1]3x3";
            strm3 = " [2]5x5";
            strm4 = " [3]10x10";
        } else if (game->dimention == 5) {
            strm2 = " [1]3x3";
            strm3 = "*[2]5x5";
            strm4 = " [3]10x10";
        } else if (game->dimention == 10) {
            strm2 = " [1]3x3";
            strm3 = " [2]5x5";
            strm4 = "*[3]10x10";
        }
        GraphicPrimitives::drawText2D(&strm1[0], 0.0f - 0.1f, 0.6f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm2[0], 0.0f - 0.1f, 0.4f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm3[0], 0.0f - 0.1f, 0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm4[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (modejeu == -9) {
        string strm11 = "Joueur1:";
        string strm12;
        string strm13;
        string strm14;
        string strm15;
        if (game->j1 == 1) {
            strm12 = "*[1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j1 == 2) {
            strm12 = " [1]Joueur";
            strm13 = "*[2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j1 == 3) {
            strm12 = " [1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = "*[3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j1 == 4) {
            strm12 = " [1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = "*[4]Ia alpha-beta";
        }
        GraphicPrimitives::drawText2D(&strm11[0], 0.0f - 0.1f, 0.6f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm12[0], 0.0f - 0.1f, 0.4f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm13[0], 0.0f - 0.1f, 0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm14[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm15[0], 0.0f - 0.1f, -0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (modejeu == -8) {
        string strm11 = "Joueur2:";
        string strm12;
        string strm13;
        string strm14;
        string strm15;
        if (game->j2 == 1) {
            strm12 = "*[1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j2 == 2) {
            strm12 = " [1]Joueur";
            strm13 = "*[2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j2 == 3) {
            strm12 = " [1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = "*[3]Ia min-max";
            strm15 = " [4]Ia alpha-beta";
        } else if (game->j2 == 4) {
            strm12 = " [1]Joueur";
            strm13 = " [2]Ia random";
            strm14 = " [3]Ia min-max";
            strm15 = "*[4]Ia alpha-beta";
        }
        GraphicPrimitives::drawText2D(&strm11[0], 0.0f - 0.1f, 0.6f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm12[0], 0.0f - 0.1f, 0.4f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm13[0], 0.0f - 0.1f, 0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm14[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm15[0], 0.0f - 0.1f, -0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (modejeu == -7) {
        string strm21 = "Joueur qui commence:";
        string strm22;
        string strm23;
        string strm24;
        string strm25;
        switch (game->j1) {
            case 1:
                strm24 = " (humain)";
                break;
            case 2:
                strm24 = " (random=)";
                break;
            case 3:
                strm24 = " (MinMax)";
                break;
            case 4:
                strm24 = " (AlphaBeta)";
                break;
            case 5:
                strm24 = " (AlphaBeta)";
                break;
            default:
                break;
        }
        switch (game->j2) {
            case 1:
                strm25 = " (humain)";
                break;
            case 2:
                strm25 = " (random=)";
                break;
            case 3:
                strm25 = " (MinMax)";
                break;
            case 4:
                strm25 = " (AlphaBeta)";
                break;
            case 5:
                strm25 = " (AlphaBeta)";
                break;
            default:
                break;
        }
        if (game->joueur == 1) {
            strm22 = "*[1]Joueur1"+strm24;
            strm23 = " [2]Joueur2"+strm25;
        } else if (game->joueur == 2) {
            strm22 = " [1]Joueur1"+strm24;
            strm23 = "*[2]Joueur2"+strm25;
        }
        GraphicPrimitives::drawText2D(&strm21[0], 0.0f - 0.1f, 0.6f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm22[0], 0.0f - 0.1f, 0.4f, 1.0f, 0.0f, 0.0f, 1.0f);
        GraphicPrimitives::drawText2D(&strm23[0], 0.0f - 0.1f, 0.2f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (modejeu == 0) {
        game->initCase();
        game->setMode(1);
    } else if (modejeu == 1) {
        game->draw();
    } else if (game->modejeu == -1) {
        string str2 = "Joueur1 gagne";
        GraphicPrimitives::drawText2D(&str2[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (game->modejeu == -2) {
        string str3 = "Joueur2 gagne";
        GraphicPrimitives::drawText2D(&str3[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (game->modejeu == -3) {
        string str4 = "Egalite";
        GraphicPrimitives::drawText2D(&str4[0], 0.0f - 0.1f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f);
    } else if (game->modejeu == 0) {

    }
}
