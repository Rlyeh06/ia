#include "Case.h"

void Case::draw(){
    if (type==0){//libre
	GraphicPrimitives::drawFillRect2D(posX,posY,taille,taille,0.0f,0.0f,0.0f);
    }else if (type==1){//joueur1
        GraphicPrimitives::drawFillRect2D(posX,posY,taille,taille,0.0f,1.0f,0.0f);
    }else if (type==2){//joueur2
        GraphicPrimitives::drawFillRect2D(posX,posY,taille,taille,1.0f,0.0f,0.0f);
    }
}
void Case::setType(int i){
    type=i;
}