#include "MyGameEngine.h"

void MyGameEngine::idle() {
    int modejeu = game->modejeu;
    if (modejeu == 1) {
        int joueur = game->joueur;
        int joueurType;
        if (joueur == 1) {
            joueurType = game->j1;
        } else if (joueur == 2) {
            joueurType = game->j2;
        }
        if (joueurType == 1) {
        } else if (joueurType == 2) {
            //std::cout << "rand" << std::endl;
            ia->random(game);
            //std::cout << "finrand" << std::endl;
        } else if (joueurType == 3) {
            //std::cout << "minmax" << std::endl;
            ia->minMax(game);
        } else if (joueurType == 4) {
            //std::cout << "alphabeta" << std::endl;
            ia->alphaBeta(game);
        }else if (joueurType == 5){
            ia->competition(game);
        }
    }
}