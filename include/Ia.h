#pragma once
#include "Jeu.h"
#include "CalcIa.h"

class Ia {
    CalcIa *calcIa;
public:
    Ia(CalcIa *calcIa_):
    calcIa(calcIa_){}
    void humain(Jeu *game,int i);
    void random(Jeu *game);
    void minMax(Jeu *game);
    void alphaBeta(Jeu *game);
    void competition(Jeu *game);
};

