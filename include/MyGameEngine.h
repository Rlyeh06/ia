#pragma once
#include "Engine.h"
#include "Jeu.h"
#include "Ia.h"

class MyGameEngine : public GameEngine {
    Jeu *game;
    Ia *ia;
public:

    MyGameEngine(Jeu *game_,
            Ia *ia_) :
    game(game_),
    ia(ia_){}
    virtual void idle();
};
