#pragma once
#include "Engine.h"
#include "Jeu.h"
#include "Ia.h"

class MyControlEngine : public ControlEngine {
    Jeu *game;
    Ia *ia;
public:

    MyControlEngine(Jeu *game_,
            Ia *ia_) :
    game(game_),
    ia(ia_) {}
    virtual void MouseCallback(int button, int state, int x, int y);
    virtual void KeyboardCallback(unsigned char key, int x, int y);
};
