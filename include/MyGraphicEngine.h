#pragma once
#include "Engine.h"
#include "GraphicPrimitives.h"
#include "Jeu.h"
#include "Ia.h"

class MyGraphicEngine : public GraphicEngine {
    Jeu *game;
    Ia *ia;
public:

    MyGraphicEngine(Jeu *game_,
            Ia *ia_):
    game(game_),
    ia(ia_){}
    virtual void Draw();
    virtual void reshape(int x, int y);

};
