#pragma once
#include "Case.h"

class Jeu {
    std::vector<Case * > *cases;
    std::vector<int> *casesLibres;
    
public:

    Jeu(std::vector<Case * > *cases_,
            std::vector<int> *casesLibres_,
            int width_ = 800,
            int height_ = 600,
            int dimention_ = 3,
            int nbgagne_ = 3,
            int modejeu_ = 1,
            int joueur_ = 1,
            int restant_ = 3 * 3,
            int j1_ = 1,
            int j2_ = 1,
            int gagnant_ = 0) :
    cases(cases_),
    casesLibres(casesLibres_),
    wWIDTH(width_),
    wHEIGHT(height_),
    dimention(dimention_),
    nbgagne(nbgagne_),
    modejeu(modejeu_),
    joueur(joueur_),
    restant(dimention_*dimention_),
    j1(j1_),
    j2(j2_),
    gagnant(gagnant_) {}
    int wWIDTH;
    int wHEIGHT;
    int dimention;
    int nbgagne;
    int modejeu;
    int joueur;
    int restant;
    int j1;
    int j2;
    int gagnant;
    void reset();
    int numPions();
    void setMode(int i);
    void getCaseLibre();
    int getNbCaseLibre();
    int numCaseLibre(int i);
    void jouerCase(int i);
    void annulerCase(int i);
    void changeJoueur();
    void caseRestant();
    void setJoueurCommence(int i);
    void setDimention(int i);
    void setNbgagne(int i);
    void setJoueur1(int i);
    void setJoueur2(int i);
    void setGagnant(int i);
    void initRestant();
    int caseVide(int i);
    int getTypeCase(int i);
    void setTypeCase(int i, int j);
    int checkCell(int numcase, int profondeur, int joueur, int dx, int dy);
    int gagne(int numcase, int joueur);
    void initCase();
    void draw();
};