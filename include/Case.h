#pragma once
#include "GraphicPrimitives.h"

class Case {
public:
	Case(float posX_ = 0.0f,
	float posY_ = 0.0f,
	float taille_=0.66f,
        int type_=0):
	posX(posX_),
	posY(posY_),
        taille(taille_),
	type(type_){}
	float posX,posY;
	float taille;
        int type;
	void draw();
        void setType(int i);
};
