#pragma once
#include "Jeu.h"

struct nombrePion {
    int nbPion;
    int nbPionJoueur;
};

class CalcIa {
public:
    CalcIa(int ia_) :
    ia(ia_) {
    }
    int ia;
    int gagne(Jeu *game,int numcase, int joueur);
    int checkCell(Jeu *game,int numcase, int profondeur, int joueur, int dx, int dy);
    void setIa(Jeu *game);
    int minMM(Jeu *game, int prof);
    int maxMM(Jeu *game, int prof);
    int minAB(Jeu *game, int prof, int alpha, int beta);
    int maxAB(Jeu *game, int prof, int alpha, int beta);
    int minABcompet(Jeu *game, int prof, int alpha, int beta);
    int maxABcompet(Jeu *game, int prof, int alpha, int beta);
    int score(nombrePion tmp);
    int evaluJeu(Jeu *game);
    int evaluJeuCompet(Jeu *game);
    nombrePion evaluationCell(Jeu *game, int numcase, int profondeur, int jouer, int dx, int dy,int serie);
    nombrePion evaluationCellCompet(Jeu *game, int numcase, int profondeur, int jouer, int dx, int dy,int serie);
    nombrePion evaluCase(int typeCase, int serie);
    nombrePion evaluCaseCompet(int typeCase, int serie);
};